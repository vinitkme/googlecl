[![Build Status](https://travis-ci.org/vinitcool76/googlecl.png?branch=master)](https://travis-ci.org/vinitcool76/googlecl)
##Google CL

Google CL is a project originally made by Google Engineers and it gives access to Google services in command line. It has been abandoned for over an year so forked it to Github to keep it alive. 


In order to contribute read the issues and solve existing bugs or send PR with the bugfix/features/enhancement. 

Please read the styleguide and contribute accordingly.



